# coding: utf-8
import json
import os

from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.core.window import Window
from kivy.graphics import Color, Rectangle
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.floatlayout import FloatLayout

from audio_registry import setup_audio_registry
from entities.registry import setup_entities_registry
from decorations.registry import setup_decorations_registry
from lib import TICK_PERIOD, solid_widgets_collide, ANIMATION, PLANE_MODES


class Level(Widget):
    horizontal_scroll = 0
    is_succeded = False
    is_failed = False

    def __init__(self, **kw):
        super(Level, self).__init__(**kw)
        self.theme = Window.audios_registry.get('main_theme')

    @property
    def player(self):
        for widget in self.entity_layout.children:
            if isinstance(widget, Window.entities_registry.classes.Player):
                return widget

    def mute_all(self):
        for sound in Window.audios_registry.values():
            if sound.status != 'stop':
                sound.stop()

    def play_sound(self, sound_name):
        sound = Window.audios_registry.get(sound_name)
        if sound:
            sound.play()

    def update_background(self):
        self.canvas.before.clear()
        with self.canvas.before:
            Color(0.2, 0.6, 255)
            Rectangle(pos=self.pos, size=self.size)
            Color(1, 1, 1)

    # frame renderer
    def update(self, dt):
        # widget physics
        self.entity_buffer = []
        trashbox = []

        # entities movement
        for entity in self.entity_layout.children:
            entity.move()
            # get screen-scrolling value
            if entity.is_centered:
                self.horizontal_scroll = self.center_x - entity.center_x

        # entities collision
        for entity in self.entity_layout.children:
            # update entity widget position
            if not entity.is_static and entity.is_solid:
                for wid in self.entity_layout.children:
                    solid_widgets_collide(entity, wid)

            # mark destroyed entities as trashed
            if entity.is_destroyed:
                trashbox.append(entity)

        # clean trashed widgets
        for trashed_widget in trashbox:
            trashed_widget.canvas.clear()
            self.entity_layout.remove_widget(trashed_widget)

        # add entities, created in this frame
        for new_entity in self.entity_buffer:
            self.entity_layout.add_widget(new_entity)

        # render decorations and entities
        self.update_background()

        for widget in self.back_decor_layout.children + self.entity_layout.children + self.fore_decor_layout.children:
            if hasattr(widget, 'previous_center'):
                # check if entity falling
                if widget.previous_center.y != widget.center_y:
                    widget.set_animation(anim_state=ANIMATION.FALL)

            widget.x += self.horizontal_scroll
            widget.update_canvas()

        # level operations
        if self.is_failed:
            self.parent.life_counter -= 1
            self.parent.load_level()

        if self.is_succeded:
            self.parent.level_num += 1
            self.parent.load_level()


class GameWidget(Widget):
    title_sign = Image(source=os.path.join('images', 'title.png'))
    current_level = None
    level_events = None

    def __init__(self, **kwargs):
        super(GameWidget, self).__init__(**kwargs)
        self.size = Window.size
        setup_audio_registry()
        setup_entities_registry()
        setup_decorations_registry()
        self.load_title_screen()

    def _run_current_level(self):
        self.remove_widget(self.loader)
        update_event = Clock.schedule_interval(self.current_level.update, TICK_PERIOD)
        self.level_events.append(update_event)
        if self.current_level.theme:
            self.current_level.theme.loop = True
            self.current_level.theme.play()

    def _load_level_pack(self):
        with open('levels.lvl', 'r') as level_file:
            self.level_pack_data = json.loads(level_file.read())

    def _level_loader(self):
        self.current_level.clear_widgets()
        # background decorations
        back_decor_layout = FloatLayout(size_hint=(None, None))
        for obj in self.level_pack_data[self.level_num]['decorations']:
            if obj['plane'] == PLANE_MODES.BACK:
                widget = Window.decorations_registry.factories[obj['type']](
                    pos=obj['position'],
                    size=obj['size'],
                    size_hint=(None, None)
                )
                back_decor_layout.add_widget(widget)
        self.current_level.add_widget(back_decor_layout)
        self.current_level.back_decor_layout = back_decor_layout

        # entities
        post_render_list = []
        entity_layout = FloatLayout(size_hint=(None, None))
        for obj in self.level_pack_data[self.level_num]['entities']:
            widget = Window.entities_registry.factories[obj['type']](
                pos=obj['position'],
                size=obj['size'],
                size_hint=(None, None),
                params=obj['params']
            )
            if isinstance(widget, Window.entities_registry.classes.Player):
                post_render_list.append(widget)
            else:
                # add widgets on backplane position
                entity_layout.add_widget(widget)

        # add widgets on foreplane position
        for widget in post_render_list:
            entity_layout.add_widget(widget)
        self.current_level.add_widget(entity_layout)
        self.current_level.entity_layout = entity_layout

        # foreground decorations
        fore_decor_layout = FloatLayout(size_hint=(None, None))
        for obj in self.level_pack_data[self.level_num]['decorations']:
            if obj['plane'] == PLANE_MODES.FORE:
                widget = Window.decorations_registry.factories[obj['type']](
                    pos=obj['position'],
                    size=obj['size'],
                    size_hint=(None, None)
                )
                fore_decor_layout.add_widget(widget)
        self.current_level.add_widget(fore_decor_layout)
        self.current_level.fore_decor_layout = fore_decor_layout

    # title screen and options menu
    def load_title_screen(self):
        self.level_num = 0
        self.life_counter = 3
        self.unload_level()
        title_layout = FloatLayout(size=self.size)
        start_btn = Button(text='START', size=(200, 100), pos=(400, 300), size_hint=(None, None))
        start_btn.bind(on_release=self.start_game)
        title_layout.add_widget(start_btn)
        with title_layout.canvas.before:
            Color(0.2, 0.6, 255)
            Rectangle(pos=title_layout.pos, size=title_layout.size)
            Color(1, 1, 1)
            Rectangle(pos=(title_layout.width * 0.1, title_layout.height * 0.7),
                      size=self.title_sign.texture.size,
                      texture=self.title_sign.texture)

        self.add_widget(title_layout)

    def start_game(self, instance):
        self.remove_widget(instance.parent)
        self._load_level_pack()
        self.load_level()

    def run_loader(self):
        spacing = 100
        loader = BoxLayout(center=(self.center_x - spacing / 2, self.center_y),
                           minimum_height=100,
                           spacing=spacing
                           )
        loader.add_widget(Label())                  # empty label to place texture
        player_icon = Window.entities_registry.types['player'].icon
        with loader.canvas:
            Rectangle(pos=(loader.pos[0], loader.center_y - player_icon.height / 2),
                      size=player_icon.size,
                      texture=player_icon)
        loader.add_widget(Label(text='[size=40]X[/size]', markup=True))
        loader.add_widget(Label(text='[size=60]{0}[/size]'.format(self.life_counter), markup=True))
        self.loader = loader
        self.add_widget(self.loader)

    def unload_level(self):
        if self.level_events:
            for event in self.level_events:
                event.cancel()
        # clear level objs
        if self.current_level:
            self.current_level.mute_all()
            self.remove_widget(self.current_level)

    def load_level(self):
        # go to title screen if last level passed
        if self.level_num > (len(self.level_pack_data) - 1):
            self.load_title_screen()
            return

        self.unload_level()
        self.level_events = []
        self.run_loader()
        Clock.schedule_once(lambda dt: self._run_current_level(), 2)
        self.current_level = Level(size=self.size)
        self._level_loader()
        self.add_widget(self.current_level)


class GameApp(App):
    """ Game itself """
    def build(self):
        # app config
        Config.set('graphics', 'resizable', '0')
        Config.write()
        Window.size = (1024, 768)
        return GameWidget()


if __name__ == '__main__':
    GameApp().run()
