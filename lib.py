# coding: utf-8
import math
from collections import namedtuple

from kivy.uix.floatlayout import FloatLayout


TICK_PERIOD = 1.0 / 58.0
MAX_GRAVITY_SPEED = 30
GRAVITY = 8.0

# level height borders
MIN_ABSOLUTE_HEIGHT = 0
MAX_ABSOLUTE_HEIGHT = 1000

# keys consts
KEY_STATE = namedtuple('Key_states', ['KEY_RELEASED', 'KEY_HOLDED'])('released', 'holded')
DIRECTIONS = namedtuple('Directions', ['UP', 'DOWN', 'RIGHT', 'LEFT'])('up', 'down', 'right', 'left')
CONTROLS = {
    'right': "move_right",
    'left': "move_left",
    'up': "jump"
}

PLANE_MODES = namedtuple('PlaneModes', ['FORE', 'BACK'])('fore', 'back')

ANIMATION = namedtuple('Animation',
                       ['STATIC', 'WALK', 'FALL', 'DEATH']
                       )('static', 'walk', 'fall', 'death')

COLLIDE_PREFIX = 'collide_'


# change collided solid widgets position
def solid_widgets_collide(mover, obstacle):
    if not (mover is not obstacle and
            mover.is_solid and
            # obstacle.is_solid and
            mover.collide_widget(obstacle)
            ):
        return

    # TODO change collision mechanism to remove +/-1 correction
    k_x = (mover.previous_center.x - obstacle.center_x) * (float(obstacle.height) / obstacle.width)
    k_y = (mover.previous_center.y - obstacle.center_y)
    obstacle_collision_side = None

    # additional condition for obj with hight height/width value
    # upper side obstacle collision
    if (k_x <= k_y and -k_x <= k_y) or (mover.center_y - obstacle.top) >= (mover.height / 4.0):
        obstacle_collision_side = DIRECTIONS.UP
        if obstacle.is_solid:
            mover.y = obstacle.top
            if mover.vertical_speed < 0:
                mover.vertical_speed = 0
                mover.time_on_air = 0
            if mover.anim_state == ANIMATION.FALL:
                mover.set_animation(anim_state=ANIMATION.STATIC)

    # right side obstacle collision
    elif (k_x >= k_y and -k_x <= k_y) or (mover.center_x - obstacle.right) >= (mover.width / 4.0):
        obstacle_collision_side = DIRECTIONS.RIGHT
        if obstacle.is_solid and (obstacle.y - mover.center_y) < (mover.height / 4.0):
            mover.x = obstacle.right + 1
            mover.speed = 0

    # left side obstacle collision
    elif (-k_x >= k_y and k_x <= k_y) or (obstacle.x - mover.center_x) >= (mover.width / 4.0):
        obstacle_collision_side = DIRECTIONS.LEFT
        if obstacle.is_solid and (obstacle.y - mover.center_y) < (mover.height / 4.0):
            mover.x = obstacle.x - mover.width - 1
            mover.speed = 0

    # lower side obstacle collision
    elif k_x > k_y and -k_x > k_y:
        obstacle_collision_side = DIRECTIONS.DOWN
        if obstacle.is_solid:
            mover.y = obstacle.y - mover.height
            if mover.vertical_speed > 0:
                mover.vertical_speed = 0
                mover.time_on_air = 0

    if hasattr(mover, COLLIDE_PREFIX + obstacle.type):
        getattr(mover, COLLIDE_PREFIX + obstacle.type)(obstacle_collision_side, obstacle)

    if hasattr(obstacle, COLLIDE_PREFIX + mover.type):
        getattr(obstacle, COLLIDE_PREFIX + mover.type)(obstacle_collision_side, mover)

    return obstacle_collision_side


class RadialLayout(FloatLayout):
    def __init__(self, **kw):
        super(FloatLayout, self).__init__(**kw)
        widgets = kw.get('widgets')
        radius = kw.get('radius', 100)
        if widgets:
            rad_interval = 2 * math.pi / len(widgets)
            rad = 0
            for widget in widgets:
                x = radius * math.cos(rad) + self.x
                y = radius * math.sin(rad) + self.y
                widget.pos = (x, y)
                widget.size = (80, 50)
                widget.size_hint = (None, None)
                self.add_widget(widget)

                rad += rad_interval
