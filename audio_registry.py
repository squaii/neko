# coding: utf-8
import os

from kivy.core.audio.audio_sdl2 import SoundSDL2
from kivy.core.window import Window


AUDIO_DIRECTORY = 'audios'


def setup_audio_registry():
    registry = {}
    for filename in os.listdir(AUDIO_DIRECTORY):
        name = filename.split('.')[0]
        path = os.path.join(AUDIO_DIRECTORY, filename)
        registry[name] = SoundSDL2(source=path)

    Window.audios_registry = registry
