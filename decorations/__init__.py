# coding: utf-8
from kivy.clock import Clock
from kivy.graphics import Rectangle
from kivy.uix.widget import Widget

from lib import ANIMATION, DIRECTIONS


class DecorationWidget(Widget):
    type = None
    image = None

    # editor data
    frame_expands = False
    icon = None

    # texturing and animation
    facing = DIRECTIONS.RIGHT
    anim_state = ANIMATION.STATIC
    current_anim_frame = 0
    anim_event = None

    def __init__(self, **kw):
        super(DecorationWidget, self).__init__(**kw)
        self.create_animation_atlas()

    @property
    def texture(self):
        texture = self.anim_atlas[self.anim_state][self.current_anim_frame]['frame'][self.facing]
        if texture.wrap == 'repeat':
            texture.uvsize = (self.width / texture.width, -self.height / texture.height)
        return texture

    @staticmethod
    def get_editor_properties(self):
        return []

    def create_animation_atlas(self):
        texture_atlas = {}
        texture = self.image.texture
        texture_atlas['static_right'] = texture
        texture_atlas['static_left'] = texture
        texture_atlas['static_left'].flip_horizontal()
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim

        self.anim_atlas = animation_atlas

    def set_animation(self, anim_state=None):
        # if calling not from sheduler and entity not dead yet set new animation type
        if anim_state is not None and self.anim_state != ANIMATION.DEATH:
            self.anim_state = anim_state
            self.current_anim_frame = 0
            if self.anim_event is not None and self.anim_event.is_triggered:
                self.anim_event.cancel()

        self.current_anim_frame += 1
        if self.current_anim_frame >= len(self.anim_atlas[self.anim_state]) - 1:
            self.current_anim_frame = 0

        current_frame = self.anim_atlas[self.anim_state][self.current_anim_frame]
        if current_frame['duration'] is not None:
            self.anim_event = Clock.schedule_once(self.set_animation, current_frame['duration'])

    def destroy(self):
        self.is_destroyed = True

    def update_canvas(self, *args):
        self.canvas.clear()
        with self.canvas:
            Rectangle(pos=self.pos, size=self.size, texture=self.texture)
