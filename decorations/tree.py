# coding: utf-8
import os

from kivy.uix.image import Image

from . import DecorationWidget


class Tree(DecorationWidget):
    type = 'tree'
    image = Image(source=os.path.join('images', 'tree.png'))

    icon = image.texture
