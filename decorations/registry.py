# coding: utf-8
from collections import namedtuple

from kivy.core.window import Window
from kivy.factory import Factory

from .cloud import Cloud
from .fence import Fence
from .tree import Tree
from .hill import Hill
from .grass import Grass


def setup_decorations_registry():

    # list of classes for app
    app_decoration_classes = (
        Cloud,
        Fence,
        Tree,
        Hill,
        Grass
    )

    decor_types = {cls.type: cls for cls in app_decoration_classes}

    decor_classes = namedtuple(
        'RegistryClasses',
        [cls.__name__ for cls in app_decoration_classes]
    )(*app_decoration_classes)

    decor_factories = {}
    for cls in app_decoration_classes:
        Factory.register(cls.__name__, cls=cls)
        decor_factories[cls.type] = getattr(Factory, cls.__name__)

    registry = namedtuple('Registry', ['types', 'classes', 'factories'])
    Window.decorations_registry = registry(decor_types, decor_classes, decor_factories)
