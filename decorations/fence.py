# coding: utf-8
import os

from kivy.uix.image import Image

from . import DecorationWidget
from lib import ANIMATION


class Fence(DecorationWidget):
    type = 'fence'
    image = Image(source=os.path.join('images', 'fence.png'))

    frame_expands = True
    icon = image.texture
    icon.wrap = 'repeat'

    def create_animation_atlas(self):
        texture_atlas = {}
        texture = self.image.texture
        texture_atlas['static_right'] = texture
        texture_atlas['static_right'].wrap = 'repeat'
        texture_atlas['static_left'] = texture
        texture_atlas['static_left'].flip_horizontal()
        texture_atlas['static_left'].wrap = 'repeat'
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim

        self.anim_atlas = animation_atlas
