# coding: utf-8
import os

from kivy.uix.image import Image

from . import DecorationWidget


class Cloud(DecorationWidget):
    type = 'cloud'
    image = Image(source=os.path.join('images', 'cloud.png'))

    icon = image.texture
