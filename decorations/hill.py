# coding: utf-8
import os

from kivy.uix.image import Image

from . import DecorationWidget


class Hill(DecorationWidget):
    type = 'hill'
    image = Image(source=os.path.join('images', 'hill.png'))

    icon = image.texture
