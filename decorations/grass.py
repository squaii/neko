# coding: utf-8
import os

from kivy.uix.image import Image

from . import DecorationWidget


class Grass(DecorationWidget):
    type = 'grass'
    image = Image(source=os.path.join('images', 'grass.png'))

    icon = image.texture
