# coding: utf-8
import os
from types import MethodType

from kivy.core.window import Window
from kivy.uix.image import Image

from . import EntityWidget
from lib import ANIMATION, COLLIDE_PREFIX, DIRECTIONS


# dynamic setting of widgets collide events for BaseEnemy class
def _create_collide_methods(cls):
    def collide_platform(self, collision_side, obstacle):
        if collision_side is DIRECTIONS.LEFT or collision_side is DIRECTIONS.RIGHT:
            self.acceleration = -self.acceleration
    # add method to object
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Platform.type
    setattr(cls, method_name, MethodType(collide_platform, None, cls))


class PoisonShroom(EntityWidget):
    type = 'poison_shroom'
    image = Image(source=os.path.join('images', 'poison_shroom.png'))
    is_solid = True
    is_static = False

    max_horizontal_speed = 1.5
    acceleration = 1.5
    facing = DIRECTIONS.RIGHT

    frame_expands = False
    icon = image.texture

    def __init__(self, **kw):
        super(PoisonShroom, self).__init__(**kw)
        if not self.__class__.is_collides_created:
            _create_collide_methods(self.__class__)

    def create_animation_atlas(self):
        texture_atlas = {}
        texture_atlas['static_right'] = self.image.texture.get_region(0, 0, 48, 50)
        texture_atlas['static_left'] = self.image.texture.get_region(0, 0, 48, 50)
        texture_atlas['static_left'].flip_horizontal()
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim
        animation_atlas[ANIMATION.FALL] = static_anim
        self.anim_atlas = animation_atlas
