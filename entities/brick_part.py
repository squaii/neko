# coding: utf-8
import os

from kivy.uix.image import Image

from . import EntityWidget
from lib import ANIMATION


class BrickPart(EntityWidget):
    type = 'brick_part'
    image = Image(source=os.path.join('images', 'brick_part.png'))
    is_solid = False
    is_static = False

    default_acceleration = 0.5
    max_horizontal_speed = 15

    frame_expands = False
    icon = image.texture

    def create_animation_atlas(self):
        texture_atlas = {}
        texture = self.image.texture
        texture_atlas['static_left'] = texture.get_region(0, 0, 35, 35)
        texture_atlas['static_right'] = texture.get_region(0, 0, 35, 35)
        texture_atlas['static_right'].flip_horizontal()
        texture_atlas['fall_left'] = texture.get_region(0, 0, 35, 35)
        texture_atlas['fall_right'] = texture.get_region(0, 0, 35, 35)
        texture_atlas['fall_right'].flip_horizontal()
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim
        fall_anim = [dict(duration=None,
                          frame=dict(right=self.texture_atlas['fall_right'],
                                     left=self.texture_atlas['fall_left'])
                          )
                     ]
        animation_atlas[ANIMATION.FALL] = fall_anim
        self.anim_atlas = animation_atlas
