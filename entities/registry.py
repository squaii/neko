# coding: utf-8
from collections import namedtuple

from kivy.core.window import Window
from kivy.factory import Factory

from .player import Player
from .platform import Platform
from .invisible_platform import InvisiblePlatform
from .disappear_platform import DisappearPlatform
from .spikes import Spikes
from .ender import Ender
from .base_enemy import BaseEnemy
from .winhouse import Winhouse
from .container_platform import ContainerPlatform
from .coin import Coin
from .flower import Flower
from .poison_shroom import PoisonShroom
from .brick_part import BrickPart
from .ground import Ground
from .block import Block


def setup_entities_registry():

    # list of classes for app
    app_entity_classes = (
        Player,
        Platform,
        InvisiblePlatform,
        DisappearPlatform,
        Spikes,
        Ender,
        BaseEnemy,
        Winhouse,
        ContainerPlatform,
        Coin,
        Flower,
        PoisonShroom,
        BrickPart,
        Ground,
        Block
    )

    entity_types = {cls.type: cls for cls in app_entity_classes}

    entity_classes = namedtuple(
        'RegistryClasses',
        [cls.__name__ for cls in app_entity_classes]
    )(*app_entity_classes)

    entity_factories = {}
    for cls in app_entity_classes:
        Factory.register(cls.__name__, cls=cls)
        entity_factories[cls.type] = getattr(Factory, cls.__name__)

    registry = namedtuple('Registry', ['types', 'classes', 'factories'])
    Window.entities_registry = registry(entity_types, entity_classes, entity_factories)
