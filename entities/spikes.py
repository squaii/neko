# coding: utf-8
import os

from kivy.uix.image import Image

from .ground import Ground


class Spikes(Ground):
    type = 'spikes'
    is_solid = False
    image = Image(source=os.path.join('images', 'spikes.png'))

    icon = image.texture
    icon.wrap = 'repeat'
