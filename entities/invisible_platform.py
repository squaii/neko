# coding: utf-8
import os

from kivy.uix.image import Image

from .container_platform import ContainerPlatform


class InvisiblePlatform(ContainerPlatform):
    type = 'invisible_platform'
    image = Image(source=os.path.join('images', 'invis_brick.png'))
    is_solid = False

    opacity = 0

    icon = image.texture
    icon.wrap = 'repeat'

    def __init__(self, **kw):
        super(InvisiblePlatform, self).__init__(**kw)

    def make_visible(self):
        self.reveal_item()
        self.canvas.opacity = 1
        self.is_solid = True
