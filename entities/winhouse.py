# coding: utf-8
import os

from kivy.uix.image import Image

from . import EntityWidget
from lib import ANIMATION


class Winhouse(EntityWidget):
    type = 'winhouse'
    image = Image(source=os.path.join('images', 'winhouse.png'))
    is_static = True

    icon = image.texture

    def create_animation_atlas(self):
        texture_atlas = {}
        texture_atlas['static_right'] = self.image.texture
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim
        self.anim_atlas = animation_atlas
