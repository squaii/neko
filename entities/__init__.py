# coding: utf-8
import math

from kivy.clock import Clock
from kivy.graphics import Rectangle
from kivy.uix.widget import Widget
from kivy.vector import Vector

from lib import MAX_GRAVITY_SPEED, MAX_ABSOLUTE_HEIGHT, MIN_ABSOLUTE_HEIGHT, TICK_PERIOD, ANIMATION, DIRECTIONS, GRAVITY


class EntityWidget(Widget):
    type = None
    image = None
    is_solid = False                # block movement on collision
    is_static = False               # movement blocked
    is_centered = False             # centering view on widget
    is_collides_created = False     # future feature
    is_destroyed = False

    # physics
    time_on_air = 0             # freefall duration
    vertical_speed = 0
    horizontal_speed = 0
    max_horizontal_speed = 0    # TODO: absolute maximum now, must be maximum for controls only
    acceleration = 0            # current horizontal acceleration
    default_acceleration = 0    # const attr for widget

    # editor data
    frame_expands = False
    icon = None

    # texturing and animation
    facing = DIRECTIONS.RIGHT       # face direction
    anim_state = ANIMATION.STATIC   # type of animation
    current_anim_frame = 0
    anim_event = None               # event for frame changing

    def __init__(self, **kw):
        super(EntityWidget, self).__init__(**kw)
        self.create_animation_atlas()
        self.set_previous_center()

    # main level widget
    @property
    def level(self):
        return super(EntityWidget, self).parent.parent

    @property
    def texture(self):
        if (
            (self.facing is DIRECTIONS.RIGHT and self.acceleration < 0) or
            (self.facing is DIRECTIONS.LEFT and self.acceleration > 0)
        ):
            self.facing = DIRECTIONS.LEFT if self.facing is DIRECTIONS.RIGHT else DIRECTIONS.RIGHT
        texture = self.anim_atlas[self.anim_state][self.current_anim_frame]['frame'][self.facing]
        # if texture.wrap == 'repeat':
        #     texture.uvsize = (self.width / texture.width, -self.height / texture.height)
        return texture

    # remembering previous center position of widget
    def set_previous_center(self):
        self.previous_center = Vector(self.center_x, self.center_y)

    def create_animation_atlas(self):
        raise Exception

    @staticmethod
    def get_editor_properties(self):
        return []

    def set_animation(self, anim_state=None):
        # if calling not from sheduler and entity not dead yet set new animation type
        if anim_state is not None and self.anim_state != ANIMATION.DEATH:
            self.anim_state = anim_state
            self.current_anim_frame = 0
            if self.anim_event is not None and self.anim_event.is_triggered:
                self.anim_event.cancel()

        self.current_anim_frame += 1
        if self.current_anim_frame >= len(self.anim_atlas[self.anim_state]) - 1:
            self.current_anim_frame = 0

        current_frame = self.anim_atlas[self.anim_state][self.current_anim_frame]
        if current_frame['duration'] is not None:
            self.anim_event = Clock.schedule_once(self.set_animation, current_frame['duration'])

    def move(self):
        if self.is_static:
            return

        self.set_previous_center()

        # gravity influence
        if self.y > MAX_ABSOLUTE_HEIGHT:
            self.y = MAX_ABSOLUTE_HEIGHT
            self.vertical_speed = 0
            self.time_on_air = 0

        elif self.y >= MIN_ABSOLUTE_HEIGHT - self.height:
            self.time_on_air += TICK_PERIOD
            # gravity acceleration
            speed_loss = math.ceil(GRAVITY * self.time_on_air / 2.0)
            if self.vertical_speed - speed_loss > -self.vertical_speed:
                # reset timer then start falling
                self.time_on_air = 0

            self.vertical_speed -= speed_loss
            if abs(self.vertical_speed) > MAX_GRAVITY_SPEED:
                self.vertical_speed = MAX_GRAVITY_SPEED if self.vertical_speed > 0 else -MAX_GRAVITY_SPEED

            self.y += self.vertical_speed
        else:
            # destroy fallen objects
            if self.y < MIN_ABSOLUTE_HEIGHT - self.height:
                self.destroy()
                return

        # horizontal movement
        self.horizontal_speed += self.acceleration
        if self.horizontal_speed > 0:
            self.horizontal_speed -= 1
            if self.horizontal_speed < 0:
                self.horizontal_speed = 0
            if self.horizontal_speed > self.max_horizontal_speed:
                self.horizontal_speed = self.max_horizontal_speed

        elif self.horizontal_speed < 0:
            self.horizontal_speed += 1
            if self.horizontal_speed > 0:
                self.horizontal_speed = 0
            if self.horizontal_speed < -self.max_horizontal_speed:
                self.horizontal_speed = -self.max_horizontal_speed

        self.x += self.horizontal_speed

    def stop_move(self):
        self.acceleration = 0
        self.horizontal_speed = 0
        self.vertical_speed = 0
        self.time_on_air = 0

    def destroy(self):
        self.stop_move()
        self.is_destroyed = True
        if self is self.level.player:
            self.level.is_failed = True

    def update_canvas(self, *args):
        self.canvas.clear()
        with self.canvas:
            Rectangle(pos=self.pos, size=self.size, texture=self.texture)
