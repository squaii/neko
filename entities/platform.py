# coding: utf-8
import os

from kivy.core.window import Window
from kivy.uix.image import Image

from . import EntityWidget
from lib import ANIMATION, DIRECTIONS


class Platform(EntityWidget):
    type = 'platform'
    image = Image(source=os.path.join('images', 'brick.png'))
    is_solid = True
    is_static = True

    frame_expands = True
    icon = image.texture
    icon.wrap = 'repeat'

    def create_animation_atlas(self):
        texture_atlas = {}
        texture_atlas['static_right'] = self.image.texture
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim
        self.anim_atlas = animation_atlas

    def destroy(self):
        super(Platform, self).destroy()
        # create 4 parts of brick on the place of destroyed brick
        item = 'brick_part'
        factory = Window.entities_registry.factories[item]
        klass = Window.entities_registry.types[item]
        params = {
            'speed_sign': [-1, -1, 1, 1],
            'facing': [DIRECTIONS.LEFT, DIRECTIONS.LEFT, DIRECTIONS.RIGHT, DIRECTIONS.RIGHT],
            'vertical_speed': [15, 20, 15, 20]
        }
        for i in xrange(4):
            brick_part = factory(pos=self.center,
                                 size=Window.entities_registry.types[item].icon.size,
                                 size_hint=(None, None))
            brick_part.vertical_speed = params['vertical_speed'][i]
            brick_part.horizontal_speed = klass.max_horizontal_speed * params['speed_sign'][i]
            brick_part.acceleration = klass.default_acceleration * params['speed_sign'][i]
            brick_part.facing = params['facing'][i]
            self.level.entity_buffer.append(brick_part)
