# coding: utf-8
import os
from types import MethodType

from kivy.core.window import Window
from kivy.clock import Clock
from kivy.uix.image import Image

from . import EntityWidget
from lib import (KEY_STATE,
                 MAX_GRAVITY_SPEED,
                 ANIMATION,
                 COLLIDE_PREFIX,
                 DIRECTIONS,
                 CONTROLS)


# dynamic setting of widgets collide events for Player class
def _create_collide_methods(cls):
    def collide_platform(self, collision_side, obstacle):
        if self.center_y > self.previous_center.y and collision_side is DIRECTIONS.DOWN:
            self.level.play_sound('break_block')
            obstacle.destroy()
            self.y -= 1
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Platform.type
    setattr(cls, method_name, MethodType(collide_platform, None, cls))

    def collide_base_enemy(self, collision_side, obstacle):
        if collision_side is DIRECTIONS.UP:
            self.level.play_sound('stomp')
            obstacle.destroy()
            self.vertical_speed = 15
        elif self.is_solid:
            self.destroy()
    # add method to object
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.BaseEnemy.type
    setattr(cls, method_name, MethodType(collide_base_enemy, None, cls))

    def collide_invisible_platform(self, collision_side, obstacle):
        if self.center_y > self.previous_center.y and collision_side is DIRECTIONS.DOWN:
            obstacle.make_visible()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.InvisiblePlatform.type
    setattr(cls, method_name, MethodType(collide_invisible_platform, None, cls))

    def collide_container_platform(self, collision_side, obstacle):
        if self.center_y > self.previous_center.y and collision_side is DIRECTIONS.DOWN:
            obstacle.reveal_item()
            self.y -= 1
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.ContainerPlatform.type
    setattr(cls, method_name, MethodType(collide_container_platform, None, cls))

    def collide_disappear_platform(self, collision_side, obstacle):
        if self.vertical_speed == 0 and collision_side is DIRECTIONS.UP:
            self.level.play_sound('break_block')
            obstacle.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.DisappearPlatform.type
    setattr(cls, method_name, MethodType(collide_disappear_platform, None, cls))

    def collide_spikes(self, collision_side, obstacle):
        self.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Spikes.type
    setattr(cls, method_name, MethodType(collide_spikes, None, cls))

    def collide_ender(self, collision_side, obstacle):
        if not self.is_ender_taken and self.y >= obstacle.y:
            if collision_side is DIRECTIONS.RIGHT:
                self.x = obstacle.x + obstacle.width * 0.8
                self.facing = DIRECTIONS.LEFT
            if collision_side is DIRECTIONS.LEFT:
                self.x = obstacle.x - self.width * 0.8
                self.facing = DIRECTIONS.RIGHT
            self.is_ender_taken = True
            self._keyboard_closed()
            self.stop_move()
            if self.level.theme:
                self.level.theme.stop()
            self.level.play_sound('stage_clear')
        elif self.is_ender_taken:
            if self.anim_state is ANIMATION.FALL:
                self.vertical_speed = -4
            else:
                if self.facing is DIRECTIONS.LEFT:
                    self.acceleration = -self.default_acceleration
                if self.facing is DIRECTIONS.RIGHT:
                    self.acceleration = self.default_acceleration
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Ender.type
    setattr(cls, method_name, MethodType(collide_ender, None, cls))

    def collide_winhouse(self, collision_side, obstacle):
        if self.is_ender_taken and (self.center_x - obstacle.center_x) < 20:
            self.level.is_succeded = True
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Winhouse.type
    setattr(cls, method_name, MethodType(collide_winhouse, None, cls))

    def collide_coin(self, collision_side, obstacle):
        self.level.play_sound('coin')
        obstacle.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Coin.type
    setattr(cls, method_name, MethodType(collide_coin, None, cls))

    def collide_flower(self, collision_side, obstacle):
        obstacle.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Flower.type
    setattr(cls, method_name, MethodType(collide_flower, None, cls))

    def collide_poison_shroom(self, collision_side, obstacle):
        self.destroy()
        obstacle.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.PoisonShroom.type
    setattr(cls, method_name, MethodType(collide_poison_shroom, None, cls))

    cls.is_collides_created = True


class Player(EntityWidget):
    type = 'player'
    image = Image(source=os.path.join('images', 'neko.png'))
    is_solid = True
    is_centered = True

    max_horizontal_speed = 10
    default_acceleration = 1.4

    icon = image.texture.get_region(0, 0, 42, 60)

    is_ender_taken = False

    def __init__(self, **kw):
        super(Player, self).__init__(**kw)
        if not self.__class__.is_collides_created:
            _create_collide_methods(self.__class__)

        self._keyboard = Window.request_keyboard(self._keyboard_closed, self, 'text')
        self._keyboard_open()

    def _keyboard_open(self):
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard.unbind(on_key_up=self._on_keyboard_up)

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] in CONTROLS.keys():
            getattr(self, CONTROLS[keycode[1]])(key_state=KEY_STATE.KEY_HOLDED)

    def _on_keyboard_up(self, keyboard, keycode):
        if keycode[1] in CONTROLS.keys():
            getattr(self, CONTROLS[keycode[1]])(key_state=KEY_STATE.KEY_RELEASED)

    def create_animation_atlas(self):
        texture_atlas = {}
        texture = self.image.texture
        texture_atlas['static_right'] = texture.get_region(0, 0, 42, 60)
        texture_atlas['static_left'] = texture.get_region(0, 0, 42, 60)
        texture_atlas['static_left'].flip_horizontal()
        texture_atlas['walk1_right'] = texture.get_region(0, 0, 42, 60)
        texture_atlas['walk1_left'] = texture.get_region(0, 0, 42, 60)
        texture_atlas['walk1_left'].flip_horizontal()
        texture_atlas['walk2_right'] = texture.get_region(42, 0, 42, 60)
        texture_atlas['walk2_left'] = texture.get_region(42, 0, 42, 60)
        texture_atlas['walk2_left'].flip_horizontal()
        texture_atlas['fall_right'] = texture.get_region(84, 0, 42, 60)
        texture_atlas['fall_left'] = texture.get_region(84, 0, 42, 60)
        texture_atlas['fall_left'].flip_horizontal()
        texture_atlas['death_right'] = texture.get_region(126, 0, 42, 60)
        texture_atlas['death_left'] = texture.get_region(126, 0, 42, 60)
        texture_atlas['death_left'].flip_horizontal()
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim

        walk_anim = [dict(duration=1,
                          frame=dict(right=self.texture_atlas['walk1_right'],
                                     left=self.texture_atlas['walk1_left'])
                          ),
                     dict(duration=1,
                          frame=dict(right=self.texture_atlas['walk2_right'],
                                     left=self.texture_atlas['walk2_left'])
                          )
                     ]
        animation_atlas[ANIMATION.WALK] = walk_anim

        fall_anim = [dict(duration=None,
                          frame=dict(right=self.texture_atlas['fall_right'],
                                     left=self.texture_atlas['fall_left'])
                          )
                     ]
        animation_atlas[ANIMATION.FALL] = fall_anim

        death_anim = [dict(duration=None,
                           frame=dict(right=self.texture_atlas['death_right'],
                                      left=self.texture_atlas['death_left'])
                           )
                      ]
        animation_atlas[ANIMATION.DEATH] = death_anim
        self.anim_atlas = animation_atlas

    def move_right(self, key_state):
        if key_state is KEY_STATE.KEY_HOLDED:
            self.acceleration = self.default_acceleration
            if self.vertical_speed == 0:
                self.set_animation(anim_state=ANIMATION.WALK)

        if key_state is KEY_STATE.KEY_RELEASED:
            self.acceleration = 0
            if self.vertical_speed == 0:
                self.set_animation(anim_state=ANIMATION.STATIC)

    def move_left(self, key_state):
        if key_state is KEY_STATE.KEY_HOLDED:
            self.acceleration = -self.default_acceleration
            if self.vertical_speed == 0:
                self.set_animation(anim_state=ANIMATION.WALK)

        if key_state is KEY_STATE.KEY_RELEASED:
            self.acceleration = 0
            if self.vertical_speed == 0:
                self.set_animation(anim_state=ANIMATION.STATIC)

    def jump(self, key_state):
        if self.anim_state is not ANIMATION.FALL and key_state is KEY_STATE.KEY_HOLDED:
            self.vertical_speed = MAX_GRAVITY_SPEED * 0.8
            self.level.play_sound('jump')

    def destroy(self):
        if self.anim_state != ANIMATION.DEATH:
            self._keyboard_closed()
            self.level.mute_all()
            self.stop_move()
            self.set_animation(anim_state=ANIMATION.DEATH)
            self.level.play_sound('neko_die')
            self.vertical_speed = MAX_GRAVITY_SPEED
            self.is_solid = False
            Clock.schedule_once(lambda dt: super(Player, self).destroy(), 3)
