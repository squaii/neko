# coding: utf-8
import os

from kivy.core.window import Window
from kivy.clock import Clock
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.image import Image

from .ground import Ground


class ContainerPlatform(Ground):
    type = 'container_platform'
    image = Image(source=os.path.join('images', 'container.png'))
    empty_image = Image(source=os.path.join('images', 'invis_brick.png'))

    icon = image.texture
    icon.wrap = 'repeat'

    is_empty = False

    def __init__(self, **kw):
        super(ContainerPlatform, self).__init__(**kw)
        self.item = kw['params'].get('item', 'coin')

    @staticmethod
    def get_editor_properties(obj):
        widgets = []
        items = (Window.entities_registry.classes.PoisonShroom.type,
                 Window.entities_registry.classes.Coin.type,
                 Window.entities_registry.classes.Flower.type)
        for item in items:
            widget = ToggleButton(text=item,
                                  group='container',
                                  on_release=lambda x: obj.params.update(item=x.text))
            if item == obj.params.get('item', 'coin'):
                widget.state = 'down'

            widgets.append(widget)
        return widgets

    def reveal_item(self):
        if not self.is_empty:
            self.is_empty = True
            if self.is_solid:
                self.image = self.empty_image
                self.create_animation_atlas()

            item_factory = Window.entities_registry.factories[self.item]
            item = item_factory(pos=(self.x, self.top),
                                size=Window.entities_registry.types[self.item].icon.size,
                                size_hint=(None, None))
            item.center_x = self.center_x
            self.level.entity_buffer.append(item)
            if self.item == 'coin':
                self.level.play_sound('coin')
                item.is_static = False
                item.vertical_speed = 15
                Clock.schedule_once(lambda dt: item.destroy(), 0.2)
