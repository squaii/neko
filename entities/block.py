# coding: utf-8
import os

from kivy.uix.image import Image

from .ground import Ground


class Block(Ground):
    type = 'block'
    image = Image(source=os.path.join('images', 'block.png'))

    icon = image.texture
    icon.wrap = 'repeat'
