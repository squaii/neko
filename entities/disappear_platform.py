# coding: utf-8
from .platform import Platform


class DisappearPlatform(Platform):
    type = 'disappear_platform'
