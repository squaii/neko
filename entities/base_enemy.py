# coding: utf-8
import os
from types import MethodType

from kivy.core.window import Window
from kivy.uix.image import Image

from . import EntityWidget
from lib import ANIMATION, COLLIDE_PREFIX, DIRECTIONS


# dynamic setting of widgets collide events for BaseEnemy class
def _create_collide_methods(cls):
    def collide_platform(self, collision_side, obstacle):
        if collision_side is DIRECTIONS.LEFT or collision_side is DIRECTIONS.RIGHT:
            self.acceleration = -self.acceleration
    # add method to object
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Platform.type
    setattr(cls, method_name, MethodType(collide_platform, None, cls))

    def collide_invisible_platform(self, collision_side, obstacle):
        if self.center_y > self.previous_center.y and collision_side is DIRECTIONS.DOWN:
            obstacle.make_visible()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.InvisiblePlatform.type
    setattr(cls, method_name, MethodType(collide_invisible_platform, None, cls))

    def collide_container_platform(self, collision_side, obstacle):
        if self.center_y > self.previous_center.y and collision_side is DIRECTIONS.DOWN:
            obstacle.reveal_item()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.ContainerPlatform.type
    setattr(cls, method_name, MethodType(collide_container_platform, None, cls))

    def collide_disappear_platform(self, collision_side, obstacle):
        if self.vertical_speed == 0 and collision_side is DIRECTIONS.UP:
            obstacle.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.DisappearPlatform.type
    setattr(cls, method_name, MethodType(collide_disappear_platform, None, cls))

    def collide_spikes(self, collision_side, obstacle):
        self.destroy()
    method_name = COLLIDE_PREFIX + Window.entities_registry.classes.Spikes.type
    setattr(cls, method_name, MethodType(collide_spikes, None, cls))

    cls.is_collides_created = True


class BaseEnemy(EntityWidget):
    type = 'base_enemy'
    image = Image(source=os.path.join('images', 'enemy.png'))
    is_solid = True

    max_horizontal_speed = 1.5
    acceleration = -1.5
    facing = DIRECTIONS.LEFT

    icon = image.texture

    def __init__(self, **kw):
        super(BaseEnemy, self).__init__(**kw)
        if not self.__class__.is_collides_created:
            _create_collide_methods(self.__class__)

    def create_animation_atlas(self):
        texture_atlas = {}
        texture = self.image.texture
        texture_atlas['static_left'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['static_right'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['static_right'].flip_horizontal()
        texture_atlas['walk_left'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['walk_right'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['walk_right'].flip_horizontal()
        texture_atlas['fall_left'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['fall_right'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['fall_right'].flip_horizontal()
        texture_atlas['death_left'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['death_right'] = texture.get_region(0, 0, 57, 48)
        texture_atlas['death_right'].flip_horizontal()
        self.texture_atlas = texture_atlas

        animation_atlas = {}
        static_anim = [dict(duration=None,
                            frame=dict(right=self.texture_atlas['static_right'],
                                       left=self.texture_atlas['static_left'])
                            )
                       ]
        animation_atlas[ANIMATION.STATIC] = static_anim

        walk_anim = [dict(duration=None,
                          frame=dict(right=self.texture_atlas['walk_right'],
                                     left=self.texture_atlas['walk_left'])
                          )
                     ]
        animation_atlas[ANIMATION.WALK] = walk_anim

        fall_anim = [dict(duration=None,
                          frame=dict(right=self.texture_atlas['fall_right'],
                                     left=self.texture_atlas['fall_left'])
                          )
                     ]
        animation_atlas[ANIMATION.FALL] = fall_anim

        death_anim = [dict(duration=None,
                           frame=dict(right=self.texture_atlas['death_right'],
                                      left=self.texture_atlas['death_left'])
                           )
                      ]
        animation_atlas[ANIMATION.DEATH] = death_anim
        self.anim_atlas = animation_atlas

    def destroy(self):
        self.stop_move()
        self.is_solid = False
        super(BaseEnemy, self).destroy()
