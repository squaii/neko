# coding: utf-8
import json
from collections import namedtuple
from math import ceil

from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.core.window import Window
from kivy.graphics import Color, Rectangle
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.dropdown import DropDown

from decorations.registry import setup_decorations_registry
from entities.registry import setup_entities_registry
from lib import TICK_PERIOD, PLANE_MODES, RadialLayout


STANDART_BUTTON_SIZE = (150, 40)
EMPTY_BUTTON_TEXT = 'empty'
FRAME_BUTTON_TEXT = 'frame'
EDIT_MODES = namedtuple('EditModes', ['ENTITY', 'DECORATION'])('entity', 'decoration')
SCROLL_SPEED = 15


class EditorInterfaceLayout(FloatLayout):
    mode = EDIT_MODES.ENTITY
    current_editable_obj = None
    frame_active = False
    plane_mode = PLANE_MODES.BACK

    def __init__(self, **kw):
        super(EditorInterfaceLayout, self).__init__(**kw)
        self.size = Window.size
        self.add_mode_button()
        self.add_frame_button()
        self.add_entities_dropdown()
        self.add_decorations_dropdown()
        self.property_menu = None

    def get_obj_class(self, obj):
        if self.mode is EDIT_MODES.ENTITY:
            return Window.entities_registry.types[obj.type]
        if self.mode is EDIT_MODES.DECORATION:
            return Window.decorations_registry.types[obj.type]

    # clear editable obj data
    def set_click_mode(self, instance=None):
        self.current_editable_obj = None
        text = EMPTY_BUTTON_TEXT if instance is None else instance.text
        if self.mode is EDIT_MODES.ENTITY:
            self.entities_dropdown.select(text)
        if self.mode is EDIT_MODES.DECORATION:
            self.decorations_dropdown.select(text)
        self.parent.touch_pos = None
        self.parent.obj_size = None

    # set editable obj data
    def set_editable_entity(self, instance):
        self.current_editable_obj = instance.text
        if self.mode is EDIT_MODES.ENTITY:
            self.entities_dropdown.select(instance.text)
            registry = Window.entities_registry
        if self.mode is EDIT_MODES.DECORATION:
            self.decorations_dropdown.select(instance.text)
            registry = Window.decorations_registry

        self.parent.obj_size = registry.types[instance.text].icon.size

    # bind interface widget to screen border (or center by default)
    def get_anchored(self, widget, x='center', y='center'):
        layout = AnchorLayout(anchor_x=x, anchor_y=y)
        layout.anchor_x = x
        layout.anchor_y = y
        layout.add_widget(widget)
        self.add_widget(layout)

    def add_entities_dropdown(self):
        dropdown = DropDown()
        entity_main_btn = Button(text=EMPTY_BUTTON_TEXT,
                                 size_hint=(None, None),
                                 size=STANDART_BUTTON_SIZE)
        entity_main_btn.bind(on_release=dropdown.open)
        dropdown.bind(on_select=lambda instance, x: setattr(entity_main_btn, 'text', x))
        self.get_anchored(entity_main_btn, x='left', y='top')

        empty_mode_btn = Button(text=EMPTY_BUTTON_TEXT,
                                size_hint_y=None,
                                height=STANDART_BUTTON_SIZE[1])
        empty_mode_btn.bind(on_release=self.set_click_mode)
        dropdown.add_widget(empty_mode_btn)
        for entity_type in Window.entities_registry.types:
            btn = Button(text=entity_type, size_hint_y=None, height=STANDART_BUTTON_SIZE[1])
            btn.bind(on_release=self.set_editable_entity)
            dropdown.add_widget(btn)

        self.entities_dropdown = dropdown

    def add_decorations_dropdown(self):
        dropdown = DropDown()
        decor_main_btn = Button(text=EMPTY_BUTTON_TEXT,
                                size_hint=(None, None),
                                size=STANDART_BUTTON_SIZE)
        decor_main_btn.bind(on_release=dropdown.open)
        dropdown.bind(on_select=lambda instance, x: setattr(decor_main_btn, 'text', x))
        self.get_anchored(decor_main_btn, x='left', y='top')

        empty_mode_btn = Button(text=EMPTY_BUTTON_TEXT,
                                size_hint_y=None,
                                height=STANDART_BUTTON_SIZE[1])
        empty_mode_btn.bind(on_release=self.set_click_mode)
        dropdown.add_widget(empty_mode_btn)
        for entity_type in Window.decorations_registry.types:
            btn = Button(text=entity_type, size_hint_y=None, height=STANDART_BUTTON_SIZE[1])
            btn.bind(on_release=self.set_editable_entity)
            dropdown.add_widget(btn)

        self.decorations_dropdown = dropdown

    def add_mode_button(self):
        mode_btn = Button(text=self.mode,
                          size_hint=(None, None),
                          size=STANDART_BUTTON_SIZE)
        mode_btn.bind(on_release=self.change_edit_mode)
        self.get_anchored(mode_btn, x='center', y='top')
        self.mode_button = mode_btn

    def add_frame_button(self):
        frame_btn = ToggleButton(text=FRAME_BUTTON_TEXT,
                                 size_hint=(None, None),
                                 size=STANDART_BUTTON_SIZE,
                                 group='frame',
                                 state='normal')
        frame_btn.bind(on_release=self.change_frame_mode)
        self.get_anchored(frame_btn, x='center', y='bottom')
        self.frame_button = frame_btn

    def add_plane_switcher(self):
        plane_switch_btn = Button(text=self.plane_mode,
                                  size_hint=(None, None),
                                  size=STANDART_BUTTON_SIZE)
        plane_switch_btn.bind(on_release=self.change_plane)
        self.get_anchored(plane_switch_btn, x='right', y='top')

    def add_obj_property_menu(self, obj):
        self.remove_property_menu()
        if obj.mode is not self.mode:
            return

        widgets = []
        remove_btn = Button(text='Remove')
        remove_btn.bind(on_release=self.remove_level_obj)
        widgets.append(remove_btn)

        obj_class = self.get_obj_class(obj)
        widgets += obj_class.get_editor_properties(obj)

        property_layout = RadialLayout(pos=Window.mouse_pos, widgets=widgets)
        property_layout.owner_obj = obj
        self.add_widget(property_layout)
        self.property_menu = property_layout

    def remove_property_menu(self):
        if self.property_menu:
            self.remove_widget(self.property_menu)
            self.property_menu = None

    # remove obj and its property menu
    def remove_level_obj(self, instance):
        property_menu = instance.parent
        if self.mode is EDIT_MODES.ENTITY:
            self.parent.remove_entity_object(property_menu.owner_obj)
        if self.mode is EDIT_MODES.DECORATION:
            self.parent.remove_decoration_object(property_menu.owner_obj)
        self.remove_property_menu()

    def change_frame_mode(self, instance=None):
        self.frame_active = not self.frame_active
        self.parent.touch_pos = None

    def change_plane(self, instance):
        self.plane_mode = PLANE_MODES.FORE if self.plane_mode is PLANE_MODES.BACK else PLANE_MODES.BACK
        instance.text = self.plane_mode

    def change_edit_mode(self, instance=None):
        self.clear_widgets()
        if self.mode is EDIT_MODES.ENTITY:
            self.mode = EDIT_MODES.DECORATION
            self.add_decorations_dropdown()
            self.add_plane_switcher()
        else:
            self.mode = EDIT_MODES.ENTITY
            self.add_entities_dropdown()

        self.add_mode_button()
        self.add_frame_button()
        self.mode_button.text = self.mode
        self.set_click_mode()

    def update(self):
        self.canvas.after.clear()
        for widget in self.children:
            with self.canvas.after:
                Color(0, 0, 0, 0)
                Rectangle(pos=widget.pos, size=widget.size)


class EditorEntityLayout(FloatLayout):
    horizontal_scroll = 0

    def __init__(self, **kw):
        super(EditorEntityLayout, self).__init__(**kw)
        self.size = Window.size

    def update(self):
        for widget in self.children:
            widget.x += self.horizontal_scroll
            # add texture for object
            texture = Window.entities_registry.types[widget.type].icon
            if texture.wrap == 'repeat':
                texture.uvsize = (ceil(widget.size[0] / texture.width),
                                  -ceil(widget.size[1] / texture.height))
            widget.canvas.clear()
            with widget.canvas:
                Color(1, 1, 1)
                Rectangle(size=widget.size, pos=widget.pos, texture=texture)

        self.horizontal_scroll = 0


class EditorDecorationLayout(FloatLayout):
    horizontal_scroll = 0

    def __init__(self, **kw):
        super(EditorDecorationLayout, self).__init__(**kw)
        self.size = Window.size

    def update(self):
        for widget in self.children:
            widget.x += self.horizontal_scroll
            # add texture for object
            texture = Window.decorations_registry.types[widget.type].icon
            if texture.wrap == 'repeat':
                texture.uvsize = (ceil(widget.size[0] / texture.width),
                                  -ceil(widget.size[1] / texture.height))
            widget.canvas.clear()
            with widget.canvas:
                Color(1, 1, 1)
                Rectangle(size=widget.size, pos=widget.pos, texture=texture)

        self.horizontal_scroll = 0


class EditorWidget(Widget):
    update_event = None
    current_level_num = None
    level_pack_data = None
    obj_size = None
    touch_pos = None
    frame_pos = None
    frame_size = None
    controls = {
        'right': "scroll_right",
        'left': "scroll_left",
        's': "save_level",
        'l': "load_level"
    }

    def __init__(self, **kw):
        super(EditorWidget, self).__init__(**kw)
        setup_entities_registry()
        setup_decorations_registry()
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self, 'text')
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._load_level_pack()
        self.add_level_menu()

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if self.current_level_num is not None and keycode[1] in self.controls.keys():
            getattr(self, self.controls[keycode[1]])()

    def _load_level_pack(self):
        with open('levels.lvl', 'r') as level_file:
            level_data = json.loads(level_file.read())
            self.level_pack_data = level_data

    def _save_level_pack(self):
        with open('levels.lvl', 'w') as level_file:
            level_file.write(json.dumps(self.level_pack_data))

    def _add_level(self, instance):
        self.level_menu.add_widget(Button(text='Level {0}'.format(len(self.level_pack_data)),
                                          on_release=self._choose_level,
                                          size_hint=(None, None)))
        self.level_menu.add_widget(Button(text='[size=80]+[/size]',
                                          on_release=self._add_level,
                                          size_hint=(None, None),
                                          markup=True))
        self.level_menu.remove_widget(instance)

        self.level_remover_menu.add_widget(
            Button(text='[size=30]X[/size]',
                   on_release=self._remove_level,
                   size_hint=(None, None),
                   markup=True))

        self.level_pack_data.append({'entities': [], 'decorations': []})
        self._save_level_pack()

    def _choose_level(self, instance):
        child_pos = self.level_menu.children.index(instance)
        self.current_level_num = len(self.level_menu.children[child_pos:]) - 1
        self.load_level()

    def _remove_level(self, instance):
        child_pos = self.level_remover_menu.children.index(instance)
        self.level_menu.remove_widget(self.level_menu.children[child_pos + 1])
        self.level_remover_menu.remove_widget(instance)

        self.level_pack_data.pop(len(self.level_remover_menu.children[child_pos:]))
        self._save_level_pack()

    def add_level_menu(self):
        pos = (50, -50)
        cols = 6
        remover_side_size = 40
        col_width = 150
        col_height = 100
        spacing = (col_width - remover_side_size, col_height - remover_side_size)

        level_menu = GridLayout(size=Window.size,
                                pos=pos,
                                cols=cols,
                                row_force_default=True,
                                row_default_height=col_height,
                                col_force_default=True,
                                col_default_width=col_width)

        level_remover_menu = GridLayout(size=Window.size,
                                        pos=(pos[0] + spacing[0], pos[1]),
                                        spacing=spacing,
                                        cols=cols,
                                        row_force_default=True,
                                        row_default_height=col_height - spacing[1],
                                        col_force_default=True,
                                        col_default_width=col_width - spacing[0])

        for level_num in xrange(len(self.level_pack_data)):
            level_menu.add_widget(Button(text='Level {0}'.format(level_num),
                                         on_release=self._choose_level,
                                         size_hint=(None, None)))
            level_remover_menu.add_widget(Button(text='[size=30]X[/size]',
                                          on_release=self._remove_level,
                                          size_hint=(None, None),
                                          markup=True))

        level_menu.add_widget(Button(text='[size=80]+[/size]',
                                     on_release=self._add_level,
                                     size_hint=(None, None),
                                     markup=True))

        self.add_widget(level_menu)
        self.level_menu = level_menu

        self.add_widget(level_remover_menu)
        self.level_remover_menu = level_remover_menu

    def create_empty_level(self):
        if self.update_event is not None:
            self.update_event.cancel()
        self.clear_widgets()

        self.update_event = Clock.schedule_interval(self.update, TICK_PERIOD)

        decoration_layout = EditorDecorationLayout()
        self.add_widget(decoration_layout)
        self.decoration_layout = decoration_layout

        entity_layout = EditorEntityLayout()
        self.add_widget(entity_layout)
        self.entity_layout = entity_layout

        interface_layout = EditorInterfaceLayout()
        self.add_widget(interface_layout)
        self.interface_layout = interface_layout

    def remove_entity_object(self, obj):
        self.entity_layout.remove_widget(obj)

    def remove_decoration_object(self, obj):
        self.decoration_layout.remove_widget(obj)

    def create_level_object(self, pos, size, params=None):
        # create object as button
        obj = Button(pos=(ceil(pos[0]), ceil(pos[1])),
                     size=size,
                     size_hint=(None, None))
        obj.type = self.interface_layout.current_editable_obj
        obj.mode = self.interface_layout.mode
        obj.bind(on_press=self.interface_layout.add_obj_property_menu)

        if obj.mode is EDIT_MODES.ENTITY:
            obj.params = params or {}
            self.entity_layout.add_widget(obj)
        if obj.mode is EDIT_MODES.DECORATION:
            obj.plane = self.interface_layout.plane_mode
            self.decoration_layout.add_widget(obj)

        self.touch_pos = None

    def on_touch_down(self, touch):
        interface_event = super(EditorWidget, self).on_touch_down(touch)      # buttons touch fix

        if self.current_level_num is not None and not interface_event:
            self.interface_layout.remove_property_menu()
            if self.interface_layout.current_editable_obj is not None:
                if not self.interface_layout.frame_active:
                    obj_pos = (touch.x - self.obj_size[0] / 2.0,
                               touch.y - self.obj_size[1] / 2.0)
                    self.create_level_object(obj_pos, self.obj_size)
                elif self.touch_pos is None:
                    self.touch_pos = (touch.x - self.obj_size[0] / 2.0,
                                      touch.y - self.obj_size[1] / 2.0)
                else:
                    obj_x_count = int(self.frame_size[0] / self.obj_size[0])
                    obj_y_count = int(self.frame_size[1] / self.obj_size[1])
                    for x in xrange(obj_x_count):
                        for y in xrange(obj_y_count):
                            obj_pos = (
                                self.frame_pos[0] + self.obj_size[0] * x,
                                self.frame_pos[1] + self.obj_size[1] * y
                            )
                            self.create_level_object(obj_pos, self.obj_size)

    def update(self, dt):
        self.decoration_layout.update()
        self.entity_layout.update()
        self.interface_layout.update()
        self.canvas.after.clear()

        # get editor frame params
        if self.interface_layout.current_editable_obj is None:
            return

        tile_width = self.obj_size[0]
        tile_height = self.obj_size[1]

        if self.touch_pos is not None:
            frame_tiles_by_x = ceil(abs(self.touch_pos[0] - Window.mouse_pos[0]) / tile_width)
            frame_tiles_by_y = ceil(abs(self.touch_pos[1] - Window.mouse_pos[1]) / tile_height)
            frame_size = (frame_tiles_by_x * tile_width, frame_tiles_by_y * tile_height)

            if Window.mouse_pos[0] < self.touch_pos[0] and Window.mouse_pos[1] > self.touch_pos[1]:
                frame_pos = (self.touch_pos[0] + tile_width - frame_size[0], self.touch_pos[1])
            elif (Window.mouse_pos[0] > self.touch_pos[0] and
                  Window.mouse_pos[1] < self.touch_pos[1]):
                frame_pos = (self.touch_pos[0], self.touch_pos[1] + tile_height - frame_size[1])
            elif (Window.mouse_pos[0] < self.touch_pos[0] and
                  Window.mouse_pos[1] < self.touch_pos[1]):
                frame_pos = (self.touch_pos[0] + tile_width - frame_size[0],
                             self.touch_pos[1] + tile_height - frame_size[1])
            else:
                frame_pos = (self.touch_pos[0], self.touch_pos[1])

            self.frame_pos = frame_pos
            self.frame_size = frame_size
        else:
            frame_pos = (Window.mouse_pos[0] - self.obj_size[0] / 2.0,
                         Window.mouse_pos[1] - self.obj_size[1] / 2.0)
            frame_size = self.obj_size

        # draw editor frame
        with self.canvas.after:
            Color(0, 1, 0, 0.4)
            Rectangle(pos=frame_pos, size=frame_size)

    def scroll_right(self):
        self.entity_layout.horizontal_scroll -= SCROLL_SPEED
        self.decoration_layout.horizontal_scroll -= SCROLL_SPEED

    def scroll_left(self):
        self.entity_layout.horizontal_scroll += SCROLL_SPEED
        self.decoration_layout.horizontal_scroll += SCROLL_SPEED

    def save_level(self):
        level_data = {'entities': [], 'decorations': []}
        for entity in self.entity_layout.children:
            level_data['entities'].append({
                'type': entity.type,
                'position': entity.pos,
                'size': entity.size,
                'params': entity.params})

        for decor in self.decoration_layout.children:
            level_data['decorations'].append({
                'type': decor.type,
                'position': decor.pos,
                'size': decor.size,
                'plane': decor.plane})

        self.level_pack_data[self.current_level_num] = level_data
        self._save_level_pack()

        print "Saved successfully."

    def load_level(self):
        self.create_empty_level()

        self.interface_layout.mode = EDIT_MODES.ENTITY
        for data in self.level_pack_data[self.current_level_num]['entities']:
            self.interface_layout.current_editable_obj = data['type']
            obj_params = data.get('params', {})
            self.create_level_object(data['position'], data['size'], obj_params)

        self.interface_layout.mode = EDIT_MODES.DECORATION
        for data in self.level_pack_data[self.current_level_num]['decorations']:
            self.interface_layout.current_editable_obj = data['type']
            self.interface_layout.plane_mode = data['plane']
            self.create_level_object(data['position'], data['size'])

        self.interface_layout.change_edit_mode()
        print "Loaded successfully."


class EditorApp(App):
    """ Level editor """
    def build(self):
        # app config
        Config.set('graphics', 'resizable', '0')
        Config.write()
        Window.size = (1024, 768)
        return EditorWidget()

if __name__ == '__main__':
    EditorApp().run()
